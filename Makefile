######################################################################
# @author      : annika (annika@blackforge)
# @file        : Makefile
# @created     : Monday May 09, 2022 14:15:28 CEST
######################################################################


test:
	go test ./pkg/...

readme:
	cp res/README.md.tmpl README.md
	gomarkdoc ./pkg/... >> README.md

