package main

import (
	"context"
	"fmt"
	"log"
	"os"

	"gitlab.com/ix-api/ix-api-client-go/pkg/ixapi"
)

func main() {
	ctx := context.Background()

	fmt.Println("ix-api list network services")
	c := ixapi.NewClient("http://localhost:8000/api/v2")

	if err := c.Authenticate(ctx, &ixapi.OAuth2ClientCredentials{
		Key:      os.Getenv("IX_API_KEY"),
		Secret:   os.Getenv("IX_API_SECRET"),
		TokenURL: os.Getenv("IX_API_OAUTH2_TOKEN_URL"),
		Scopes:   []string{"ix-api"},
	}); err != nil {
		log.Fatal(err)
	}

	// Network services
	ns, err := c.NetworkServicesList(ctx)
	if err != nil {
		log.Fatal(err)
	}
	for _, n := range ns {
		// We are only interested in exchange lan network services
		fmt.Println("Network Service:", n)
	}
}
