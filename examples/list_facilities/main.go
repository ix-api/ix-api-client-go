package main

import (
	"context"
	"fmt"
	"log"

	"gitlab.com/ix-api/ix-api-client-go/pkg/ixapi"
)

func main() {
	ctx := context.Background()

	fmt.Println("ix-api list network services")

	c := ixapi.NewClient("http://localhost:8000/api/v2")

	if err := c.Authenticate(ctx, &ixapi.AuthAPIKeySecret{
		Key:    "<api key>",
		Secret: "<secret>",
	}); err != nil {
		log.Fatal(err)
	}

	// List facilities
	qry := &ixapi.FacilitiesListQuery{
		ID: []string{"15", "17", "21"},
	}
	facilities, err := c.FacilitiesList(ctx, qry)
	if err != nil {
		log.Fatal(err)
	}

	for _, f := range facilities {
		fmt.Println("ID:", f.ID, f.Name, f.PostalCode)
	}
}
