package main

import (
	"context"
	"fmt"
	"log"
	"os"

	"gitlab.com/ix-api/ix-api-client-go/pkg/ixapi"
)

func main() {
	ctx := context.Background()

	fmt.Println("ix-api list network services")

	c := ixapi.NewClient("http://localhost:8000/api/v2")

	if err := c.Authenticate(ctx, &ixapi.AuthAPIKeySecret{
		Key:    os.Getenv("IX_API_KEY"),
		Secret: os.Getenv("IX_API_SECRET"),
	}); err != nil {
		log.Fatal(err)
	}

	// Network services
	ns, err := c.NetworkServicesList(ctx)
	if err != nil {
		log.Fatal(err)
	}
	for _, n := range ns {
		// We are only interested in exchange lan network services
		ens, ok := n.(*ixapi.ExchangeLanNetworkService)
		if !ok {
			continue
		}
		// Get required features for network services
		nfs, err := c.NetworkFeaturesList(ctx, &ixapi.NetworkFeaturesListQuery{
			NetworkService: ens.ID,
			Required:       "true",
		})
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println("Required network features for exchange-lan:", ens.Name)
		for _, nf := range nfs {
			rsnf, ok := nf.(*ixapi.RouteServerNetworkFeature)
			if !ok {
				continue
			}
			fmt.Println(" * ", rsnf.Name, "ID:", rsnf.ID)
		}
	}
}
